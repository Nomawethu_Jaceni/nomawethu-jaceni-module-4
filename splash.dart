import 'package:flutter/material.dart';

class Splash extends StatelessWidget {
  const Splash({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: "My Splash Screen",
      home: Scaffold(
        appBar: AppBar(
          title: const Text("My Splash Screen"),
        ),
        body: const Center(
          child: Text("data"),
        ),
      ),
    );
  }
}
